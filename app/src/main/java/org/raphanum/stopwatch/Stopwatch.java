package org.raphanum.stopwatch;

import java.util.ArrayList;

public class Stopwatch {

    private long mStartTime;
    private long mElapsedTime;
    private long mLapStartTime;
    private long mLapElapsedTime;
    private Status mStatus;
    private ArrayList<Long> mLaps;

    public enum Status { STARTED, PAUSED, NOT_STARTED }

    public Stopwatch() {
        mElapsedTime = 0;
        mLapElapsedTime = 0;
        mStatus = Status.NOT_STARTED;
        mLaps = new ArrayList<>();
    }

    public void start() {
        mStartTime = System.currentTimeMillis() - mElapsedTime;
        mLapStartTime = System.currentTimeMillis() - mLapElapsedTime;
        mStatus = Status.STARTED;
    }

    public void stop() {
        mElapsedTime = System.currentTimeMillis() - mStartTime;
        mLapElapsedTime = System.currentTimeMillis() - mLapStartTime;
        mStatus = Status.PAUSED;
    }

    public void reset() {
        mElapsedTime = 0;
        mLapElapsedTime = 0;
        mLaps.clear();
        mStatus = Status.NOT_STARTED;
    }

    public void addLap() {
        mLaps.add(getLapTime());
        mLapStartTime = System.currentTimeMillis();
    }

    public ArrayList<Long> getLaps() {
        return mLaps;
    }

    public long getLapTime() {
        if (mStatus == Status.STARTED) {
            mLapElapsedTime = System.currentTimeMillis() - mLapStartTime;
        }
        return mLapElapsedTime;
    }

    public long getElapsedTime() {
        if (mStatus == Status.STARTED) {
            mElapsedTime = System.currentTimeMillis() - mStartTime;
        }
        return mElapsedTime;
    }

    public Status getStatus() {
        return mStatus;
    }
}
