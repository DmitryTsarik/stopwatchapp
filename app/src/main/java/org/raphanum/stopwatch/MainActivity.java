package org.raphanum.stopwatch;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private StopwatchService mStopwatchService;
    private StopwatchService.StopwatchBinder binder = null;
    private boolean mBound = false;
    private static final long mFrequency = 100;
    private Stopwatch.Status mStopwatchStatus;

    private Button startStopButton;
    private Button resetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            mStopwatchStatus = Stopwatch.Status.values()[savedInstanceState.getInt("mStopwatchStatus")];
            binder = (StopwatchService.StopwatchBinder)savedInstanceState.getBinder("binder");
        } else {
            mStopwatchStatus = Stopwatch.Status.NOT_STARTED;
        }

        startStopButton = (Button)findViewById(R.id.start_stop_button);
        resetButton = (Button)findViewById(R.id.reset_lap_button);
        Intent serviceIntent = new Intent(this, StopwatchService.class);
        startService(serviceIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        bindService(new Intent(this, StopwatchService.class), mConnection, BIND_AUTO_CREATE);
        updateElapsedTimeView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mStopwatchService != null) {
            mStopwatchStatus = mStopwatchService.getStopwatchStatus();
        }
        outState.putInt("mStopwatchStatus", mStopwatchStatus.ordinal());
        outState.putBinder("binder", binder);
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (binder == null) {
                binder = (StopwatchService.StopwatchBinder) service;
            }
            mStopwatchService  = binder.getStopwatchService();
            mStopwatchStatus = mStopwatchService.getStopwatchStatus();
            setButtonsState();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    public void onStartStopClick(View view) {
        if (mStopwatchService.getStopwatchStatus() != Stopwatch.Status.STARTED) {
            startStopButton.setText(getResources().getString(R.string.stop));
            resetButton.setText(getResources().getString(R.string.lap));
            mStopwatchService.startStopwatch();
            resetButton.setEnabled(true);
        } else {
            startStopButton.setText(getResources().getString(R.string.start));
            resetButton.setText(getResources().getString(R.string.reset));
            mStopwatchService.stopStopwatch();
        }
    }

    public void onResetClick(View view) {
        if (mStopwatchService.getStopwatchStatus() == Stopwatch.Status.PAUSED) {
            resetButton.setEnabled(false);
            mStopwatchService.resetStopwatch();
        } else {
            //mStopwatchService.addLap();
            Toast.makeText(this, "Lap button disabled", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateElapsedTimeView() {
        final TextView elapsedTime = (TextView)findViewById(R.id.elapsed_time_view);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                String time = "";
                if (mStopwatchService != null) {
                    time = mStopwatchService.getElapsedTime();
                }
                elapsedTime.setText(time);
                handler.postDelayed(this, mFrequency);
            }
        });
    }

    private void setButtonsState() {
        switch (mStopwatchStatus) {
            case STARTED:
                startStopButton.setText(getResources().getString(R.string.stop));
                resetButton.setText(getResources().getString(R.string.lap));
                resetButton.setEnabled(true);
                break;

            case PAUSED:
                startStopButton.setText(getResources().getString(R.string.start));
                resetButton.setText(getResources().getString(R.string.reset));
                resetButton.setEnabled(true);
                break;

            case NOT_STARTED:
                resetButton.setEnabled(false);
                break;
        }
    }
}
