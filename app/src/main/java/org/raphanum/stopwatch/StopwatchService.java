package org.raphanum.stopwatch;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

public class StopwatchService extends Service {

    private static final String TAG = "Test Service";
    public static final int STOPWATCH_NOTIFICATION_ID = 321;
    private static final long mFrequency = 100;
    private final IBinder mBinder = new StopwatchBinder();
    private Stopwatch mStopwatch = new Stopwatch();
    private NotificationManager mNotificationManager;
    private Notification.Builder mNotification;

    public class StopwatchBinder extends Binder {
        StopwatchService getStopwatchService() {
            return StopwatchService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        Log.v(TAG, "onCreate " + hashCode());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createNotification();
        Log.v(TAG, "onStartCommand " + hashCode());
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mNotificationManager.cancel(STOPWATCH_NOTIFICATION_ID);
        Log.v(TAG, "onDestroy " + hashCode());
    }

    public void startStopwatch() {
        mStopwatch.start();
        updateNotification();
    }

    public void stopStopwatch() {
        mStopwatch.stop();
        stopForeground(true);
    }

    public void resetStopwatch() {
        mStopwatch.reset();
    }

    public void addLap() {
        mStopwatch.addLap();
    }

    public ArrayList<Long> getLaps() {
        return mStopwatch.getLaps();
    }

    public String getElapsedTime() {
        return getElapsedTimeString(mStopwatch.getElapsedTime());
    }

    public String getLapTime() {
        return getElapsedTimeString(mStopwatch.getLapTime());
    }

    private String getElapsedTimeString(long time) {
        long hours = time / 3600000;
        long minutes = (time - hours * 3600000) / 60000;
        long seconds = (time / 1000) % 60;
        long splitSeconds = (time % 1000) / 100;
        if (hours < 1) {
            return String.format("%02d:%02d.%01d", minutes, seconds, splitSeconds);
        }
        return String.format("%01d:%02d:%02d.%01d", hours, minutes, seconds, splitSeconds);
    }

    private void createNotification() {
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotification = new Notification.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_timer_white_48dp)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent);
    }

    private void updateNotification() {
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (mStopwatch.getStatus() == Stopwatch.Status.STARTED) {
                    mNotification.setContentText(getElapsedTime());
                    startForeground(STOPWATCH_NOTIFICATION_ID, mNotification.build());
                }
                handler.postDelayed(this, mFrequency);
            }
        });
    }

    public Stopwatch.Status getStopwatchStatus() {
        return mStopwatch.getStatus();
    }
}
